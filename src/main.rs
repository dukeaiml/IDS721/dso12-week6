use lambda_http::{run, Body, Error, Request, RequestExt, Response};
use lambda_runtime::service_fn;
use tracing_subscriber::FmtSubscriber;
use serde_json;
use tracing::{info, Level};

#[derive(Debug, serde::Serialize)]
struct BMIResponse {
    bmi: f64,
    category: &'static str,
}

async fn bmi_handler(event: Request) -> Result<Response<Body>, Error> {
    // Extract weight and height from query parameters
    let weight: f64 = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("weight"))
        .and_then(|weight| weight.parse().ok())
        .unwrap_or(0.0);

    let height: f64 = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("height"))
        .and_then(|height| height.parse().ok())
        .unwrap_or(0.0);

    // Calculate BMI height in meters and weight in kg 
    let bmi: f64 = if height != 0.0 {
        weight / (height * height)
    } else {
        0.0
    };

    // Determine BMI category
    let category = if bmi < 18.5 {
        "Underweight"
    } else if bmi < 24.9 {
        "Normal weight"
    } else if bmi < 29.9 {
        "Overweight"
    } else {
        "Obese"
    };

    // Prepare the response with BMI and category
    let response = BMIResponse { bmi, category };
    
    // Serialize the response into JSON
    let response_body = serde_json::to_string(&response)?;


    // Return the response
    Ok(Response::builder()
        .status(200)
        .header("content-type", "text/html")
        //.body(Body::from(format!("BMI: {:.2}\nCategory: {}", response.bmi, response.category)))
        .body(Body::from(response_body))
        .expect("Failed to build response"))
}



#[tokio::main]
async fn main() -> Result<(), Error> {
    // Set up the logger
    let subscriber = FmtSubscriber::builder().with_max_level(Level::INFO).finish();

    tracing::subscriber::set_global_default(subscriber).expect("Failed to set up global default logger");

    info!("Initializing the service...");


    let func = service_fn(bmi_handler);
    run(func).await?;
    Ok(())
}
